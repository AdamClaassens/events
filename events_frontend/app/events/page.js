'use client'

import { useState, useEffect } from 'react'
import CustomCard from '../(components)/card/page'

const Events = () => {
    const [events, setEvents] = useState()

    const GetEvents = async () => {
        await fetch('http://localhost:8000/api/events/')
            .then((response) => response.json())
            .then((data) => {
                console.log(data)
                setEvents(data)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    useEffect(() => {
        GetEvents()
    }, [])

    return (
        <div className=''>
            <div className='flex justify-center p-4'>
                <h1 className='text-2xl'>Events</h1>
            </div>
            <div className='flex flex-wrap gap-6 p-4'>
                {events && events.map((event) => {
                    return (
                        <div className='grow w-60 h-60'>
                            <CustomCard
                                key={event.id}
                                event_name={event.event_name}
                                datetime={event.datetime}
                                description={event.description}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Events