import Image from 'next/image'
import Events from './events/page'
import Navbar from './(components)/navbar/page'

export default function Home() {
  return (
    <main>
      <Navbar />
      <Events />
    </main>
  )
}
