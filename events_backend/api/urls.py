from django.contrib import admin
from django.urls import path

from .views import EventViewSet, EventDetailViewSet, VenueViewSet, VenueDetailViewSet

urlpatterns = [
    path('events/', EventViewSet.as_view(), name='events'),
    path('events/<int:pk>/', EventDetailViewSet.as_view(), name='event-detail'),
    path('venues/', VenueViewSet.as_view(), name='venues'),
    path('venues/<int:pk>/', VenueDetailViewSet.as_view(), name='venue-detail'),
]