from django.db import models

# Create your models here.
class Event(models.Model):
    event_name = models.CharField(max_length=100)
    datetime = models.DateTimeField()
    description = models.TextField()
    venue = models.ForeignKey('Venue', on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.event_name


class Venue(models.Model):
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self) -> str:
        return self.name