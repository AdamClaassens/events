from django.contrib import admin
from .models import Event, Venue

# Register your models here.
admin.register(Event)
admin.register(Venue)